import { Injectable } from '@angular/core';
import { command } from "@shared/shared.interface";

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  commandList: command[] = [];
  constructor() {
    this.getLocalCommandData();
  }

  /**
   * save the command list in local storage
   * @param commandList command list
   */
  setLocalCommmandData(commandList) {
    this.commandList = [...commandList];
    localStorage.setItem("commandList", JSON.stringify(this.commandList));
  }

  /**
   * get the command list form the local storage
   */
  getLocalCommandData() {
    let localData = localStorage.getItem('commandList');
    if (localData) {
      this.commandList = JSON.parse(localData);
      console.debug('command list', this.commandList);
    }
  }

  /**
   * clear the command form the local storage
   * @returns empty array
   */
  clearCommand() {
    this.commandList = [];
    localStorage.removeItem('commandList');
    return [];
  }
}
