/** Define the chess board dimension properties */
export interface boardDimension {
    x: any,
    y: any,
    whiteColor: any,
    blackColor: any
}

/** Define the sqare properties */
export interface square {
    type: string,
    color: string,
    pawnColor: string,
    activeStatus: boolean,
    facing: string,
}

/** Define the command properties */
export interface command {
    command: string,
    moveX?: number,
    moveY?: number,
    rotation?: string,
    pawncolor?: string,
    moves?: number,
    sequence?: number,
    validStatus?: boolean,
    activeStatus?: boolean,
    message?: string
}

/** Define the current command status properties */
export interface currentCommandStatus {
    x: any,
    y: any,
    oldX: any,
    oldY: any,
    rotate: any,
    pawanColor: any,
    pawanPlaceStatus?: boolean,
    pawanFirstMoveStatus?: boolean
}