
export enum DefaultConfigration {
    dimensionX = 8,
    dimensionY = 8,
    blackSquareColor = '#000',
    whiteSquareColor = '#eee',
    whitePawn = '#ffff',
    blackPawn = '#ffff',
}

export enum RotationIcon {
    NORTH = 'chevron-up-outline',
    SOUTH = 'chevron-down-outline',
    EAST = 'chevron-forward-outline',
    WEST = 'chevron-back-outline',
}

export let CommandMaster: any[] = [
    'PLACE',
    'MOVE',
    'LEFT',
    'RIGHT',
    // 'ROTATE',
    'REPORT',
];

export let RotateMaster: any[] = [
    'NORTH',
    'EAST',
    'SOUTH',
    'WEST',
];
