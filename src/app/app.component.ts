import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Setting', url: '/features/Setting', icon: 'paper-plane' },
    { title: 'Board', url: '/features/Board', icon: 'trash' },
    { title: 'Report', url: '/features/Report', icon: 'warning' },
  ];
  // public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];
  constructor() { }
}
