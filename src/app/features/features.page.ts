import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModalController, ToastController } from '@ionic/angular';
import { AddCommandComponent } from "@app/features/add-command/add-command.component";
import { boardDimension, square, command, currentCommandStatus } from "@shared/shared.interface";
import { DefaultConfigration, RotateMaster } from "@shared/shared.enum";
import { SharedService } from "@shared/shared.service";

@Component({
  selector: 'app-features',
  templateUrl: './features.page.html',
  styleUrls: ['./features.page.scss'],
})
export class FeaturesPage implements OnInit {
  public features: string;
  public dimension: boardDimension = {
    x: DefaultConfigration.dimensionX,
    y: DefaultConfigration.dimensionY,
    blackColor: DefaultConfigration.blackSquareColor,
    whiteColor: DefaultConfigration.whiteSquareColor,
  };
  public chessBoardSet: square[][];
  public commandList: command[] = [];
  public currentCommandStaus: currentCommandStatus = {
    x: '', y: '', rotate: '', pawanColor: '',
    oldX: '', oldY: '',
    pawanFirstMoveStatus: false,
    pawanPlaceStatus: false
  };

  constructor(private toast: ToastController, public modelObj: ModalController, public service: SharedService) { }

  ngOnInit() {
    this.initChessBoard();
    this.commandList = this.service.commandList;
  }

  /**
   * Init chess board, 
   * Create the 2 dimission arrray as per the dimission mention below
   * @returns void
   */
  initChessBoard(): void {
    try {

      this.chessBoardSet = [];
      for (let yIndex = 0; yIndex < this.dimension.y; yIndex++) {
        let tempRowVal: square[] = [];
        for (let xIndex = 0; xIndex < this.dimension.x; xIndex++) {
          tempRowVal.push(this.settingSquare(yIndex, xIndex))
        }
        this.chessBoardSet.push(tempRowVal);
      }
    } catch (error) {
      console.error('init Chess Board ->', error);
    }
  }

  /**
   * Set the property of each square as per the odd and even setup
   * @param yIndex Y index of array
   * @param xIndex X index of array
   * @returns square object
   */
  settingSquare(yIndex: number, xIndex: number): square {
    let tempSquare: square = { activeStatus: false, facing: '', pawnColor: 'red', color: '', type: '' };
    if (yIndex % 2) {
      tempSquare.color = (xIndex % 2 == 1) ? this.dimension.blackColor : this.dimension.whiteColor;
      tempSquare.type = (xIndex % 2 == 1) ? 'black' : 'white';
    } else {
      tempSquare.color = (xIndex % 2 == 0) ? this.dimension.blackColor : this.dimension.whiteColor;
      tempSquare.type = (xIndex % 2 == 0) ? 'black' : 'white';
    }
    return tempSquare;
  }

  /**
   * Add command for execeution 
   * Once the model is closed then the command list is updated.
   * @returns modelPresent
   */
  async addCommand() {
    try {

      const addModel = await this.modelObj.create({
        component: AddCommandComponent,
        mode: 'md', swipeToClose: true,

      });
      addModel.onDidDismiss().then(close => {
        this.commandList = [...this.service.commandList];
      })
      return await addModel.present();

    } catch (error) {
      console.error('add command', error);
    }
  }

  runCommandStatus: boolean = false;
  /**
   * Execute the list of commands,
   * Along with listing error if found.
   */
  async runCommands() {
    try {
      this.runCommandStatus = true;
      for (let index = 0; index < this.commandList.length; index++) {
        try {
          let validObj = await this.validateCommand(this.commandList[index]);
          this.commandList[index].activeStatus = true;
          this.commandList[index].validStatus = true;
          this.commandList[index].message = 'Valid';
          if (this.commandList[index].command == 'REPORT') {
            this.commandList[index].moveX = this.currentCommandStaus.x;
            this.commandList[index].moveY = this.currentCommandStaus.y;
            this.commandList[index].rotation = this.currentCommandStaus.rotate;
            this.commandList[index].pawncolor = this.currentCommandStaus.pawanColor;
          }
          this.clearNAssignNewInstruction(this.commandList[index]);
        } catch (error) {
          this.commandList[index].activeStatus = true;
          this.commandList[index].validStatus = false;
          this.commandList[index].message = error;
        }
      }
    } catch (error) {
      console.error('running command error', error);
      let toastObj = await this.toast.create({
        message: 'Oops something went wrong.',
        duration: 2000, color: 'danger'
      });
      toastObj.present();
    }
  }

  /**
   * clear the list of command and clear the pawan from the board
   */
  async clear() {
    this.commandList = this.service.clearCommand();
    if (this.currentCommandStaus.pawanPlaceStatus == true) {
      this.chessBoardSet[this.currentCommandStaus.y][this.currentCommandStaus.x].activeStatus = false;
      this.chessBoardSet[this.currentCommandStaus.y][this.currentCommandStaus.x].facing = '';
      this.chessBoardSet[this.currentCommandStaus.y][this.currentCommandStaus.x].pawnColor = '';
    }

    this.currentCommandStaus = {
      x: '', y: '', oldX: '', oldY: '', rotate: '', pawanColor: '',
      pawanFirstMoveStatus: false,
      pawanPlaceStatus: false
    };
    this.runCommandStatus = false;
  }

  /**
   * Validate the command one at a time. Also set command current status as well.
   * Currently this function is validating PLACE | MOVE | LEFT | RIGHT command
   * 
   * @param command command which need to be validated
   * @returns resolve -> command is valid or reject with reason
   */
  async validateCommand(command: command): Promise<any> {
    return new Promise((resolve, reject) => {
      try {
        let initPlaceCommand: boolean = false;

        if (command.command == 'PLACE') {

          if (this.currentCommandStaus.pawanPlaceStatus == true) {
            this.currentCommandStaus.oldX = this.currentCommandStaus.x;
            this.currentCommandStaus.oldY = this.currentCommandStaus.y;
          }
          this.currentCommandStaus.x = command.moveX;
          this.currentCommandStaus.y = command.moveY;
          this.currentCommandStaus.rotate = command.rotation;
          this.currentCommandStaus.pawanColor = command.pawncolor;
          this.currentCommandStaus.pawanPlaceStatus = true;
        }

        if (command.command == 'MOVE') {
          if (this.currentCommandStaus.pawanFirstMoveStatus == true && command.moves > 1) {
            throw 'Can\'t move greater then 1 step.'
          }

          let newX: number = this.currentCommandStaus.x;
          let newY: number = this.currentCommandStaus.y;
          switch (this.currentCommandStaus.rotate) {
            case 'NORTH':
              newY = Number(newY) - Number(command.moves);
              break;
            case 'SOUTH':
              newY = Number(newY) + Number(command.moves);
              break;
            case 'EAST':
              newX = Number(newX) + Number(command.moves);
              break;
            case 'WEST':
              newX = Number(newX) - Number(command.moves);
              break;
          }

          if (typeof this.chessBoardSet[newY] == 'undefined') {
            throw 'Can\'t move pawan at Y axis. As it\'s going out of bound.';
          } else if (typeof this.chessBoardSet[newY][newX] == 'undefined') {
            throw 'Can\'t move pawan at X axis. As it\'s going out of bound.';
          }
          this.currentCommandStaus.oldX = this.currentCommandStaus.x;
          this.currentCommandStaus.oldY = this.currentCommandStaus.y;
          this.currentCommandStaus.x = newX;
          this.currentCommandStaus.y = newY;
          this.currentCommandStaus.pawanFirstMoveStatus = true;
        }

        if (command.command == 'RIGHT') {
          let index = RotateMaster.findIndex((val) => val == this.currentCommandStaus.rotate);
          if (index != -1) {
            if (index != (RotateMaster.length - 1)) {
              this.currentCommandStaus.rotate = RotateMaster[index + 1];
            } else {
              this.currentCommandStaus.rotate = RotateMaster[0];
            }
          }
        }

        if (command.command == 'LEFT') {
          let index = RotateMaster.findIndex((val) => val == this.currentCommandStaus.rotate);
          if (index != -1) {
            if (index == 0) {
              this.currentCommandStaus.rotate = RotateMaster[3];
            } else {
              this.currentCommandStaus.rotate = RotateMaster[index - 1];
            }
          }
        }

        resolve(true);
      } catch (error) {
        reject(error.toString());
      }
    });
  }

  /**
   * Clear the previous action or command and write new command on the square
   * @param command current command
   * @returns false
   */
  clearNAssignNewInstruction(command: command) {
    if (this.currentCommandStaus.oldY != '') {
      this.chessBoardSet[this.currentCommandStaus.oldY][this.currentCommandStaus.oldX].activeStatus = false;
      this.chessBoardSet[this.currentCommandStaus.oldY][this.currentCommandStaus.oldX].facing = '';
      this.chessBoardSet[this.currentCommandStaus.oldY][this.currentCommandStaus.oldX].pawnColor = '';
    }

    this.chessBoardSet[this.currentCommandStaus.y][this.currentCommandStaus.x].activeStatus = true;
    this.chessBoardSet[this.currentCommandStaus.y][this.currentCommandStaus.x].facing = this.currentCommandStaus.rotate;
    this.chessBoardSet[this.currentCommandStaus.y][this.currentCommandStaus.x].pawnColor = this.currentCommandStaus.pawanColor;
    return false;
  }

  /**
   * show message if the command is invalid on to the toaster.
   * @param commandDetails comandDetails
   */
  async showMessage(commandDetails: command) {
    if (commandDetails.validStatus == false && commandDetails.activeStatus == true) {
      let toastObj = await this.toast.create({
        message: commandDetails.message,
        duration: 2000, color: 'danger'
      });
      toastObj.present();
    }
  }

  isModalOpen: boolean = false;
}
