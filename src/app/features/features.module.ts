import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { IonicModule } from '@ionic/angular';
import { FeaturePageRoutingModule } from './features-routing.module';
import { FeaturesPage } from './features.page';
import { PawnDirective } from "./pawn.directive";
import { AddCommandComponent } from "./add-command/add-command.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule,
    IonicModule,
    FeaturePageRoutingModule
  ],
  declarations: [FeaturesPage, PawnDirective, AddCommandComponent],
  exports: [PawnDirective]
})
export class FeaturesPageModule { }
