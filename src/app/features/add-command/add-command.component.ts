import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController } from "@ionic/angular";
import { SharedService } from "@shared/shared.service";
import { command } from "@shared/shared.interface";
import { CommandMaster, RotateMaster, DefaultConfigration } from "@shared/shared.enum";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-command',
  templateUrl: './add-command.component.html',
  styleUrls: ['./add-command.component.scss'],
})
export class AddCommandComponent implements OnInit {

  public commandList: command[] = [];
  public commandMaster: any[] = CommandMaster;
  public rotateMaster: any[] = RotateMaster;
  public addCommandForm: FormGroup;

  constructor(public toast: ToastController, public modalCtrl: ModalController, public service: SharedService, public fb: FormBuilder) { }

  ngOnInit() {
    this.commandList = this.service.commandList;
    this.initAddCommandForm();
  }

  /**
   * init the add command form
   */
  initAddCommandForm() {
    this.addCommandForm = this.fb.group({
      'command': ['', [Validators.required]],
      'positionX': ['', [Validators.min(0), Validators.max(DefaultConfigration.dimensionX)]],
      'positionY': ['', [Validators.min(0), Validators.max(DefaultConfigration.dimensionY)]],
      'rotation': ['', []],
      'pawanColor': ['', []],
      'moves': ['', [Validators.max(2)]],
    });
    if (this.commandList.length == 0) {
      this.addCommandForm.get('command').setValue('PLACE');
      this.addCommandForm.get('command').disable();
      this.commandChange();
    }
  }

  /**
   * save the command in to the list. 
   * Before saving it perform validation as well
   */
  async saveCommand() {
    try {

      if (this.addCommandForm.invalid)
        throw 'Please enter valid data.';

      this.commandList.push({
        command: this.addCommandForm.get('command').value,
        moveX: this.addCommandForm.get('positionX').value,
        moveY: this.addCommandForm.get('positionY').value,
        rotation: this.addCommandForm.get('rotation').value,
        pawncolor: this.addCommandForm.get('pawanColor').value,
        moves: this.addCommandForm.get('moves').value,
        activeStatus: false,
        sequence: this.commandList.length + 1,
        message: '',
        validStatus: false
      });

      this.addCommandForm.get('command').enable();
      this.service.setLocalCommmandData(this.commandList);
      this.addCommandForm.reset();

    } catch (error) {
      console.error('save command error', error);
      let toastObj = await this.toast.create({
        message: error.toString(),
        duration: 2000,
        color: 'danger'
      });
      toastObj.present();
    }
  }

  /**
   * Close the model
   */
  dismissModel() {
    this.modalCtrl.dismiss();
  }

  /**
   * Base on the command selected the validation is updated in to the form
   */
  commandChange() {
    switch (this.addCommandForm.get('command').value) {
      case 'PLACE':
        this.addCommandForm.get('positionX').addValidators(Validators.required);
        this.addCommandForm.get('positionX').updateValueAndValidity();
        this.addCommandForm.get('positionY').addValidators(Validators.required);
        this.addCommandForm.get('positionY').updateValueAndValidity();
        this.addCommandForm.get('rotation').addValidators(Validators.required);
        this.addCommandForm.get('rotation').updateValueAndValidity();
        this.addCommandForm.get('moves').removeValidators(Validators.required);
        this.addCommandForm.get('moves').updateValueAndValidity();
        break;
      case 'MOVE':
        this.addCommandForm.get('positionX').removeValidators(Validators.required);
        this.addCommandForm.get('positionX').updateValueAndValidity();
        this.addCommandForm.get('positionY').removeValidators(Validators.required);
        this.addCommandForm.get('positionY').updateValueAndValidity();
        this.addCommandForm.get('rotation').removeValidators(Validators.required);
        this.addCommandForm.get('rotation').updateValueAndValidity();
        this.addCommandForm.get('moves').addValidators(Validators.required);
        this.addCommandForm.get('moves').updateValueAndValidity();
        break;
      case 'ROTATE':
        this.addCommandForm.get('positionX').removeValidators(Validators.required);
        this.addCommandForm.get('positionX').updateValueAndValidity();
        this.addCommandForm.get('positionY').removeValidators(Validators.required);
        this.addCommandForm.get('positionY').updateValueAndValidity();
        this.addCommandForm.get('rotation').addValidators(Validators.required);
        this.addCommandForm.get('rotation').updateValueAndValidity();
        this.addCommandForm.get('moves').removeValidators(Validators.required);
        this.addCommandForm.get('moves').updateValueAndValidity();
        break;
      case 'LEFT':
        this.addCommandForm.get('positionX').removeValidators(Validators.required);
        this.addCommandForm.get('positionX').updateValueAndValidity();
        this.addCommandForm.get('positionY').removeValidators(Validators.required);
        this.addCommandForm.get('positionY').updateValueAndValidity();
        this.addCommandForm.get('rotation').removeValidators(Validators.required);
        this.addCommandForm.get('rotation').updateValueAndValidity();
        this.addCommandForm.get('moves').removeValidators(Validators.required);
        this.addCommandForm.get('moves').updateValueAndValidity();
        break;
      case 'RIGHT':
        this.addCommandForm.get('positionX').removeValidators(Validators.required);
        this.addCommandForm.get('positionX').updateValueAndValidity();
        this.addCommandForm.get('positionY').removeValidators(Validators.required);
        this.addCommandForm.get('positionY').updateValueAndValidity();
        this.addCommandForm.get('rotation').removeValidators(Validators.required);
        this.addCommandForm.get('rotation').updateValueAndValidity();
        this.addCommandForm.get('moves').removeValidators(Validators.required);
        this.addCommandForm.get('moves').updateValueAndValidity();
        break;
      case 'REPORT':
        this.addCommandForm.get('positionX').removeValidators(Validators.required);
        this.addCommandForm.get('positionX').updateValueAndValidity();
        this.addCommandForm.get('positionY').removeValidators(Validators.required);
        this.addCommandForm.get('positionY').updateValueAndValidity();
        this.addCommandForm.get('rotation').removeValidators(Validators.required);
        this.addCommandForm.get('rotation').updateValueAndValidity();
        this.addCommandForm.get('moves').removeValidators(Validators.required);
        this.addCommandForm.get('moves').updateValueAndValidity();
        break;
      default:

        break;
    }
  }
}
